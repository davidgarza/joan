=== Jock on air now ===
Contributors: ganddser  
Plugin URI: http://gandenterprisesinc.com/jock-on-air-now/
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=97GZ5SG95638N
Tags: schedule, programming, webcast, radio, Ajax, jquery, DJ, radio station
Requires at least: 3.2
Tested up to: 4.6
Stable tag: 4.2.3
Author URI: https://gandenterprisesinc.com
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Joan allows you to create, manage and display your station weekly programming schedule stylishly and easily. 


== Description ==
IMPORTANT NOTICE: If you have version 4.1.1 or lower please upgrade to version 4.1.2 as this fixes a couple of deprecated WordPress Functions. Along with a few minor text format changes, also we added a new function and shortcode.

Joan plugin  lets site admins easily manage and display a rotating programming schedule for radio or TV. Jock on air now(Joan) displays the name of the current show, DJ or host in a given time-slot. It also displays the show/jock for the next time-slot. If nothing is scheduled, it displays a custom message.

Current Features 

*Easily create and edit shows/events.

*Customizable "Off Air Message".

*Hide or show next show/event.

*Customizable CSS in the admin panel.

*Turn Off Joan.

*Display your schedule with On Air Now widget or shortcode.

*Add URL links to shows .

*Upload images for events/shows and display them in the On Air Now widget.

*Add the On Air Now widget to Posts and Pages.

*New shortcode to display scheduled shows/events for each day.

*Supports databases that don't use wp_ prefix.

== Installation ==

Download and install using the standard WordPress plugin installation method then activate the Plugin from Plugins page.

Find Jock on air now in the Admin menu click to begin adding or editing your schedule.

You must, Set your timezone and time format to 24 hour (See WordPress General).

== Frequently Asked Questions ==

Q: I just installed Joan, and I don't see where to add new shows

A: You \*\*must\*\* set your timezone first, the plugin has to have a local timezone to set its clock to. Go to your General Settings page to adjust also set time format to 24/hr.

Q: After I upload an image and click "Use as Featured Image" for a show, nothing happens

A: You have to click the "Insert Into Post" button, \*not\* Use as Featured Image 

Q: I get a destination already exists error, plugin install failed, why?

A: You should deactivate and then delete any previous versions of the the plugin then install this version.

Q. Plugin could not be activated because it triggered a fatal error.

A. Most likely you installed this version before removing the previously installed version  3.2.1 or lower. Deactivate and delete it and try activating this one again.

== Screenshots ==

1. This screenshot shows a newly added show in Admin.

2. Shows what the full schedule and current show, jock looks like on the front end using the shortcode and widget respectively.

3. Start adding images for your shows.

4. Select or upload an image also remember to choose the appropriate image size, we recommend medium if your image is bigger than 300px.

5. Save changes, image immediately shows on frontend once the time comes.

6. Reload the the settings page and expand the view to see the image next the show right away in admin.

== Display Options ==

Display the current Jock/Show in a sidebar. Find the Joan Widget in your Appearance->Widgets settings, add to your sidebar 
or add [joan-now-playing] to any page.

Display your stations Weekly Schedule. Create a new page, name it Full Schedule place the schedule shortcode save and add to your site navigation.[joan-schedule]

Automatically display your schedule for each day of the week. (All credit to Salpetriere for this one).
[schedule-today]


== Changelog ==
4.2.3

*No major changes. Works with WP 4.6 

4.2.2

* Added new screenshots for adding images

4.2.1

*Fixed add media/images in WP posts and pages
*Fixes JS conflict in admin pages

4.1.1

* Added ability to show schedule for each day e.g. What's on today - Monday (Thanks Henry).
  
* Version 4.1.1 Fixes all deprecated WordPress Functions

4.0

*Upgrade to version 4.0

*This is a major upgrade, you absolutely MUST save a copy of your current schedule and delete the previously installed version before upgrading as it will wipeout the exsiting database and start a new. You'll also get an error because WP will be looking for a file that no longer exists.

*You may need to replace your schedule display short code to this [joan-schedule]. This is because with re-wrote the database and the plugin and removed all refernces to the old database. This applies only to those upgrading from 3.2.1 or lower to 4.0.

*Wipes old database, starts fresh, saves database from there going forward

*Removed Template Tag option, we feel the standard display otpions are sufficeint

3.2.1 

* Fixed issue in crud declaration.

* Fixed security issue  

3.1.2 

* Added plugin icon, screenshots

3.1.1 

* Improved plugin code 

* Properly enqueued jQuery

== Upgrade Notice ==
*

== See Joan In Action ==

Caribbean Music <a href="http://tdnradio.net/" target="_blank">TDN Radio</a>