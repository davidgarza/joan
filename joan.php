<?php

/*
Plugin Name:  Jock On Air Now
Plugin URI: https://gandenterprisesinc.com/jock-on-air-now/ 
Description: JOAN allows you to create, manage and display your station's programming schedule quite easily. After install use the widget to display the current show/Jock on air, display full station schedule by inserting the included shortcode into posts or pages. Create a new page, name it schedule or whatever you like place the schedule shortcode on it, add to your menu to display your full schedule.
Author: G n D Enterprises, Inc.
Version: 4.2.3
Author URI: http://www.gandenterprisesinc.com

*/
if (!defined('ABSPATH')) {
	exit("Sorry, you are not allowed to access this page directly.");
}

/* Set constant for plugin directory */
define( 'SS3_URL', WP_PLUGIN_URL.'/joan' );

$joan_db_version = "3.1.1";
if (!isset($wpdb)) $wpdb = $GLOBALS['wpdb'];
$joanTable = $wpdb->prefix . "WPJoan";

$tz = get_option('timezone_string');
date_default_timezone_set($tz);

//Installation

function joan_install () {
   global $wpdb;
   global $joan_db_version;
   global $joanTable;

   $joanTable = $wpdb->prefix . "WPJoan";


   if($wpdb->get_var("show tables like '$joanTable'") != $joanTable) {
      
      $sql = "CREATE TABLE " . $joanTable . " (
	  id int(9) NOT NULL AUTO_INCREMENT,
	  dayOfTheWeek text NOT NULL,
	  startTime int(11) NOT NULL,
	  endTime int(11) NOT NULL,
	  startClock text not null,
	  endClock text not null,
	  showName text NOT NULL,
	  linkURL text NOT null,
	  imageURL text not null,
	  UNIQUE KEY id (id)
	);";

      require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
      dbDelta($sql);
 
      add_option("joan_db_version", $joan_db_version);

   }

}

register_activation_hook(__FILE__,'joan_install'); 

//Register and create the Widget

class JoanWidget extends WP_Widget
{
 /**
  * Declares the JoanWidget class.
  *
  */
    function JoanWidget(){
    $widget_ops = array('classname' => 'joan_widget', 'description' => __( "Display your schedule with style.") );
    $control_ops = array('width' => 300, 'height' => 300);
	parent::__construct( 'Joan', __( 'Joan', 'joan' ), $widget_ops, $control_ops );
    }

  /**
    * Displays the Widget
    *
    */
    function widget($args, $instance){
      extract($args);
      $title = apply_filters('widget_title', empty($instance['title']) ? '&nbsp;' : $instance['title']);

      # Before the widget
      echo $before_widget;

      # The title
      if ( $title )
      echo $before_title . $title . $after_title;

      # Make the Joan widget
      echo showme_joan();

      # After the widget
      echo $after_widget;
  }

  /**
    * Saves the widgets settings.
    *
    */
    function update($new_instance, $old_instance){
      $instance = $old_instance;
      $instance['title'] = strip_tags(stripslashes($new_instance['title']));
      $instance['lineOne'] = strip_tags(stripslashes($new_instance['lineOne']));
      $instance['lineTwo'] = strip_tags(stripslashes($new_instance['lineTwo']));

    return $instance;
  }

  /**
    * Creates the edit form for the widget.
    *
    */
    function form($instance){
      //Defaults
      $instance = wp_parse_args( (array) $instance, array('title'=>'On Air Now') );

      $title = htmlspecialchars($instance['title']);

      # Output the options
      echo '<p style="text-align:right;"><label for="' . $this->get_field_name('title') . '">' . __('Title:') . ' <input style="width: 250px;" id="' . $this->get_field_id('title') . '" name="' . $this->get_field_name('title') . '" type="text" value="' . $title . '" /></label></p>';
        }

}

  function JoanInit() {
  register_widget('JoanWidget');
  }
  add_action('widgets_init', 'JoanInit'); 


//==== OPTIONS ====

add_option('joan_upcoming', 'yes');
add_option('joan_use_images', 'yes');
add_option('joan_shutitdown', 'no');
add_option('off_air_message', 'We are currently off the air.');
add_option('joan_css', '');
add_option('joan_shutitdown', 'no');


//==== SHORTCODES ====

function joan_schedule_handler($atts, $content=null, $code=""){
if (!isset($wpdb)) $wpdb = $GLOBALS['wpdb'];
	global $wpdb;
	global $joanTable;

	//Get the current schedule, divided into days
	$daysOfTheWeek = array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");

	$schedule = array();

	$output = '';

	foreach ($daysOfTheWeek as $day) {
		if (!isset($wpdb)) $wpdb = $GLOBALS['wpdb'];
		//Add this day's shows HTML to the $output array
		$showsForThisDay =  $wpdb->get_results( $wpdb->prepare ( "SELECT * FROM $joanTable WHERE dayOfTheWeek = %s ORDER BY startTime", $day ));

		//Check to make sure this day has shows before saving the header
		if ($showsForThisDay){
			$output .= '<h2>'.$day.'</h2>';
			$output .= '<ul class="joan-schedule">';
			foreach ($showsForThisDay as $show){
				$showName = $show->showName;
				$startClock = $show->startClock;
				$endClock = $show->endClock;
				$linkURL = $show->linkURL;

				if ($linkURL){
					$showName = '<a href="'.$linkURL.'">'.$showName.'</a>';
				}

				$output .= '<li><strong>'.$startClock.'</strong> - <strong>'.$endClock.'</strong>: '.$showName.'</li>';

			}
			$output .= '</ul>';
		}
	}
	return $output;
}

add_shortcode('joan-schedule', 'joan_schedule_handler');

//Henry custom stuff

function joan_schedule_today($atts, $content=null, $code=""){

	global $wpdb;
	global $joanTable;

	//Get the current schedule, divided into days
	$daysOfTheWeek = array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");

        $today = date('l');

	$schedule = array();

	$output = '';

	foreach ($daysOfTheWeek as $day) {
		//Add this day's shows HTML to the $output array
		$showsForThisDay =  $wpdb->get_results( $wpdb->prepare ( "SELECT * FROM $joanTable WHERE dayOfTheWeek = %s ORDER BY startTime", $day ));

        if ($day == $today) {

		//Check to make sure this day has shows before saving the header
		if ($showsForThisDay){
			$output .= '<h2 class="widget-title">What\'s On Today - '.$today.'</h2>';
			$output .= '<ul class="joan-schedule">';
			foreach ($showsForThisDay as $show){
				$showName = $show->showName;
				$startClock = $show->startClock;
				$endClock = $show->endClock;
				$linkURL = $show->linkURL;
				if ($linkURL){
					$showName = '<a href="'.$linkURL.'">'.$showName.'</a>';
				}
				$output .= '<li><span class="show-time">'.$startClock./*' - '.$endClock.*/':</span> <span class="show-name">'.$showName.'</span></li>';
				}
				$output .= '</ul>';
			}
		}
	}
	return $output;
}

add_shortcode('schedule-today', 'joan_schedule_today');

//End Henry custom stuff

function showme_joan(){

    $output = '<style>'.get_option('joan_css', '').'</style>';
    $output .= '<div class="joan-now-playing"></div>';
    return $output;

}

add_shortcode('joan-now-playing', 'showme_joan');


if ( is_admin() ){ // admin actions
	add_action('admin_menu', 'joan_plugin_menu');
}


function joan_image_upload_scripts() {
wp_enqueue_script('media-upload');
wp_enqueue_script('thickbox');
wp_enqueue_script('my-upload');
}
 
function joan_image_upload_styles() {
wp_enqueue_style('thickbox');
}
 
if (isset($_GET['page']) && $_GET['page'] == 'joan_settings') {
add_action('admin_print_scripts', 'joan_image_upload_scripts');
add_action('admin_print_styles', 'joan_image_upload_styles');
}

//==== ADMIN OPTIONS AND SCHEDULE PAGE ====

function joan_plugin_menu() {
	global $pagenow;
    // Add a new submenu under Options:
	add_menu_page('JOAN', 'Jock On Air Now', 'activate_plugins', 'joan_settings', 'joan_options_page');

	// Modified by PHP Stack
    if ($pagenow == 'admin.php' && isset($_GET['page'])) {
		if ($_GET['page'] == 'joan_settings') {
		    wp_enqueue_style( "joan-admin",  plugins_url('admin.css', __FILE__) );
		    wp_enqueue_script( "joan-admin", plugins_url('admin.js', __FILE__), array('jquery'), '1.0.0', true );
		}
	}
}

function joan_options_page(){
if (!isset($wpdb)) $wpdb = $GLOBALS['wpdb'];
	global $wpdb;
	global $joanTable;
	//Check to see if the user is upgrading from an old Joan database

	if (isset($_POST['upgrade-database'])){
		if (check_admin_referer('upgrade_joan_database', 'upgrade_joan_database_field')){

			if ($wpdb->get_var("show tables like '$joanTable'") != $joanTable){
				$sql = "CREATE TABLE " . $joanTable . " (
					  id int(9) NOT NULL AUTO_INCREMENT,
					  dayOfTheWeek text NOT NULL,
					  startTime int(11) NOT NULL,
					  endTime int(11) NOT NULL,
					  startClock text not null,
					  endClock text not null,
					  showName text NOT NULL,
					  linkURL text NOT null,
					  imageURL text not null,
					  UNIQUE KEY id (id)
					);";

				      $wpdb->query($sql);
			}
			
			$joanOldTable = $wpdb->prefix.'joan';

			$oldJoanShows = $wpdb->get_results($wpdb->prepare("SELECT id, showstart, showend, showname, linkUrl, imageUrl FROM $joanOldTable WHERE id != %d", -1));
			if ($oldJoanShows){
				foreach ($oldJoanShows as $show){
					$showname = $show->showname;
					$startTime = $show->showstart;
					$endTime = $show->showend;
					$startDay = date('l', $startTime);
					$startClock = date('g:i a', ($startTime));
					$endClock = date('g:i a', ($endTime));
					$linkURL = $show->linkUrl;
					if ($linkURL == 'No link specified.'){
						$linkURL = '';
					}
					$imageURL = $show->imageUrl;

					//Insert the new show into the New Joan Databse
					$wpdb->query( $wpdb->prepare("INSERT INTO $joanTable (dayOfTheWeek, startTime,endTime,startClock, endClock, showName,  imageURL, linkURL) VALUES (%s, %d, %d , %s, %s, %s, %s, %s)", $startDay, $startTime, $endTime, $startClock, $endClock, $showname, $imageURL, $linkURL )	);
				}
			}
		}
		//Remove the old Joan table if the new table has been created
		if($wpdb->get_var("show tables like '$joanTable'") == $joanTable) {
			$wpdb->query("DROP TABLE $joanOldTable");
		}
	}
        
	// echo '<script type="text/javascript" src="'.SS3_URL.'/admin.js" ></script>';

?>
<div class="wrap">
		<div class="joan-message-window">Message goes here.</div>
		<h1>Jock On Air Now</h1>
		<p><em>Let your listeners know when their favorite jock/show is on or about to come on.</em><br /><small>by <a href='http://www.gandenterprisesinc.com' target='_blank'>G and Enterprises, Inc.</a></small></p>

		<?php
			//Check to see if Joan 2.0 is installed
		 	$table_name = $wpdb->prefix . "joan";
		   if($wpdb->get_var("show tables like '$table_name'") == $table_name) {
		   	?>
		   		<div class="error">
		   			<form method="post" action="">
		   				<p><strong>Previous version of Joan detected.</strong> Be sure to back up your database before performing this upgrade. <input type="submit" class="button-primary" value="Upgrade my Joan Database" /></p>
		   				<input type="hidden" name="upgrade-database" value=' ' />
						<?php wp_nonce_field('upgrade_joan_database', 'upgrade_joan_database_field'); ?>
		   			</form>
		   		</div>
		   	<?php
		   }

		?>

		<input type="hidden" class="script-src" readonly="readonly" value="<?= $_SERVER['PHP_SELF']; ?>?page=joan_settings" />
		<?php wp_nonce_field('delete_joan_entry', 'delete_entries_nonce_field'); ?>

		<ul class="tab-navigation">
			<li class="joan-scheudle">Schedule</li>
			<li class="joan-options">Display Options</li>
			<li class="shut-it-down" style="border:none;">Turn Off</li>
		</ul>
		<div class="joan-tabs">

			<div class="tab-container" id="joan-schedule">

				<h2>Schedule</h2>

				<h3>Add new Show</h3>

				<div class="add-new-entry">

					<form id="add-joan-entry" method="post" action="<?php echo get_admin_url()?>admin-ajax.php">
					<input type='hidden' name='action' value='show-time-curd' />
						<div class="set-joan-show-deets">
				            <div class="show-time-container">
					            <h3>Show Start</h3>
					           <label for="start">
						           	<select class="startDay" name="sday">
						          	 	<option value="Sunday">Sunday</option>
						            	<option value="Monday">Monday</option>
						            	<option value="Tuesday">Tuesday</option>
						            	<option value="Wednesday">Wednesday</option>
						            	<option value="Thursday">Thursday</option>
						            	<option value="Friday">Friday</option>
						            	<option value="Saturday">Saturday</option>
						            </select>
					        	</label> 
					            
					            <label for="starttime">
					            <input id="starttime" class="text" name="startTime" size="5" maxlength="5" type="text" value="00:00" /></label>
				            </div>
				            <div class="show-time-container">
					            <h3>Show End</h3> 
					           	<label for="endday">
						           	<select class="endDay" name="eday">
						           		<option value="Sunday">Sunday</option>
						            	<option value="Monday">Monday</option>
						            	<option value="Tuesday">Tuesday</option>
						            	<option value="Wednesday">Wednesday</option>
						            	<option value="Thursday">Thursday</option>
						            	<option value="Friday">Friday</option>
						            	<option value="Saturday">Saturday</option>
						            </select>
						        </label> 

					            <label for="endtime">
					            <input id="endtime" class="text" name="endTime" size="5" maxlength="5" type="text" value="00:00" /></label>
				             </div>
				             <div class="clr"></div>
				             <p><strong>Be sure to use 24 hour time format (01:00 = 1 AM, 13:00 = 1 PM)</strong></p>
				             <p>Also, make sure your <a href="/wp-admin/options-general.php">timezone <strong>city</strong></a> is set appropriately.(Select the city name the best matches your local time(Not the UTC)<br/>
				             	<small><em>Current timezone: <?php if (get_option('timezone_string') == '') { echo '<strong style="color:red;">Set your <a href="options-general.php">timezone</a> city now.</strong></em></small></p>'; } else { echo get_option('timezone_string'); ?></em></small></p>
				            
				        </div>
				        <div class="set-joan-show-deets">
				            <label for="showname"><h3>Show Details</h3></label> 
				            <p>Name: <br/>
				           		<input id="showname" type="text" name="showname" class="show-detail" />
							</p>
							
				            <p>Link URL (optional):<br />
								<label for="linkUrl">
				            
								<input type="text" name="linkUrl" placeholder="No URL specified." class="show-detail" />
								
							</p>
							
							<p id="primary-image"></p>
							<p><input class="image-url" type="hidden" name="imageUrl" data-target-field-name="new show" value=""/></p>
							<p><input type="button" class="upload-image button" data-target-field="new show" value="Upload Image" /></p>			
							<img src="" style="display:none;" data-target-field-name="new show" />
							<p><a id="remove-primary-image" href="#"><small>Remove Image</small></a></p>
							
								
				            <input type="submit" class="button-primary" style="cursor: pointer;" value="Add Show" />
				            <input type="hidden" name="crud-action" value="create" />
				            <?php wp_nonce_field('add_joan_entry', 'joan_nonce_field'); ?>

				            <?php } ?>

				        </div>
				    </form>

				<div class="clr"></div>

				</div>

				<h3>Current Schedule</h3>

				<p>Display type: <a href="#" class="display-toggle full-display">Expanded</a> | <a href="#" class="display-toggle simple-display">Basic</a></p>

				<form method="post" action="<?php echo get_admin_url()?>admin-ajax.php" class="joan-update-shows">
				<input type='hidden' name='action' value='show-time-curd' />
					<div class="joan-schedule loading">
						<div class="sunday-container"><h2>Sunday</h2></div><!-- end this day of the week -->
						<div class="monday-container"><h2>Monday</h2></div><!-- end this day of the week -->
						<div class="tuesday-container"><h2>Tuesday</h2></div><!-- end this day of the week -->
						<div class="wednesday-container"><h2>Wednesday</h2></div><!-- end this day of the week -->
						<div class="thursday-container"><h2>Thursday</h2></div><!-- end this day of the week -->
						<div class="friday-container"><h2>Friday</h2></div><!-- end this day of the week -->
						<div class="saturday-container"><h2>Saturday</h2></div><!-- end this day of the week -->
					</div>
				<input type="hidden" name="crud-action" value="update" />
				<?php wp_nonce_field('save_joan_entries', 'joan_entries_nonce_field'); ?>

				</form>
				
				<p>Display type: <a href="#" class="display-toggle full-display">Expanded</a> | <a href="#" class="display-toggle simple-display">Basic</a></p>


			</div>

			<div class="tab-container" id="joan-options">
			<form method="post" action="">
				<h2>Options</h2>
				<?php

				//Save posted options

				if (isset($_POST['joan_options'])){


					update_option('joan_upcoming', $_POST['joan_options']['showUpcoming']);
					update_option('joan_use_images', $_POST['joan_options']['imagesOn']);
					update_option('off_air_message', htmlentities(stripslashes($_POST['joan_options']['offAirMessage'])));
					update_option('joan_css', htmlentities(stripslashes($_POST['joan_options']['joanCSS'])));

				} else if (isset($_POST['shut-it-down'])) {
					update_option('joan_shutitdown', $_POST['shut-it-down']);
				}

				//Set options variables
				$showUpcoming = get_option('joan_upcoming');
				$imagesOn = get_option('joan_use_images');
				$shutItDown = get_option('joan_shutitdown');
				$offAirMessage = get_option('off_air_message');
				$joanCSS = get_option('joan_css');
				$shutItDown = get_option('joan_shutitdown');

				?>

				<h3>Display Images</h3>
					<form id="option" method="post" action="">
					<p>Show accompanying images with joans?</p>
						<label><input type="radio"<?php if($imagesOn == 'yes') { ?> checked="checked"<?php } ?> name="joan_options[imagesOn]" value="yes" /> : Yes</label><br/>
						<label><input type="radio"<?php if($imagesOn == 'no') { ?> checked="checked"<?php } ?> name="joan_options[imagesOn]" value="no" /> : No</label><br/>


					<h3>Upcoming Timeslot</h3>
					    
					<p>Show the name/time of the next timeslot?</p>
						<label><input type="radio"<?php if($showUpcoming == 'yes') { ?> checked="checked"<?php } ?> name="joan_options[showUpcoming]" value="yes" /> : Yes</label><br/>
						<label><input type="radio"<?php if($showUpcoming == 'no') { ?> checked="checked"<?php } ?> name="joan_options[showUpcoming]" value="no" /> : No</label><br/>


					<h3>Off Air Message</h3>
						<label>Message:<br /><input type="text" id="off-air-message" value="<?= $offAirMessage; ?>" name="joan_options[offAirMessage]" size="40" /></label>

	    
				    <p class="submit">
						<input type="submit" class="button-primary" value="Save Changes" />
					</p>

		    <h2>Display Shortcodes</h2>
			<h3>[joan-schedule]</h3>
				<p>Display a list of the times and names of your events, broken down weekly, example:</p>
				<div style="margin-left:30px;">
				<h4>Mondays</h4>
					<ul>
						<li><strong>5:00 am - 10:00 am</strong> - Morning Ride</li>
						<li><strong>10:00 am - 12:00 pm</strong> - The Vibe with MarkD</li>
					</ul>
					<h4>Saturdays</h4>
					<ul>
						<li><strong>10:00 am - 1:00 am</strong> - Drive Time</li>
						<li><strong>1:00 pm - 4:00 pm</strong> - Kool Jamz</li>
					</ul>
				</div>
			<h3>[joan-now-playing]</h3>
				<p>Display the Current Show/jock widget.</p>
			<h3>[schedule-today]</h3>
            <p>Automatically display your schedule for each day of the week.</p>			
				
		   	<div class="clr"></div>
			<h2>CSS</h2>

		    <textarea name="joan_options[joanCSS]" cols="90" rows="30"><?= $joanCSS ?></textarea>    

		    <p class="submit">
		    <input type="submit" class="button-primary" value="Save Changes" />
		    </p>
		    </form>


			</div>

			<div class="tab-container" id="joan-shut-it-down">
				
				<h2>Turn Off Joan</h2>

				<form method="post" action="">
					<p>You can temporarily take down your schedule for any reason, public holidays etc.</p>
				    	<label><input type="radio"<?php if($shutItDown == 'yes') { ?> checked="checked"<?php } ?> name="shut-it-down" value="yes" /> : Yes, Off Air.</label><br/>
						<label><input type="radio"<?php if($shutItDown == 'no') { ?> checked="checked"<?php } ?> name="shut-it-down" value="no" /> : No, we're on air (Default). </label><br/>
				    
				    <p class="submit">
				    	<input type="submit" class="button-primary" value="Save changes" />
					</p>
			 	</form>

			</div>

		</div><!-- end the joan tabs -->

	</div>

<?php

}

function joan_header_scripts(){


    echo '<script>crudScriptURL = "'.get_admin_url().'admin-ajax.php"</script>';
    wp_enqueue_script( "joan-front", plugins_url('joan.js', __FILE__), array(), '1.0.0', true );
    

}

add_action("wp_head","joan_header_scripts");
add_action('wp_ajax_show-time-curd', '_handle_form_action');
add_action('wp_ajax_nopriv_show-time-curd', '_handle_form_action');
function _handle_form_action(){
    
    include( plugin_dir_path( __FILE__ ) . 'crud.php' );

    die(); // this is required to return a proper result
}
?>
